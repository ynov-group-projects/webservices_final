-- CreateEnum
CREATE TYPE "TodoPriorityEnum" AS ENUM ('LOW', 'MEDIUM', 'HIGH');

-- CreateEnum
CREATE TYPE "TodoStatusEnum" AS ENUM ('TO_START', 'IN_PROGRESS', 'COMPLETED');

-- AlterTable
ALTER TABLE "CategoryTodo" ADD COLUMN "color" TEXT NOT NULL DEFAULT '#FFFFFF';

-- AlterTable
ALTER TABLE "Todo"
    ADD COLUMN "dueDate" TIMESTAMP(3),
ADD COLUMN "priority" "TodoPriorityEnum" NOT NULL DEFAULT 'MEDIUM',
ADD COLUMN "status" "TodoStatusEnum" NOT NULL DEFAULT 'TO_START',
ADD COLUMN "userId" INTEGER;

-- Update existing Todos with a userId
UPDATE "Todo" SET "userId" = "CategoryTodo"."userId"
    FROM "CategoryTodo"
WHERE "Todo"."categoryId" = "CategoryTodo"."id";

-- Now make userId NOT NULL
ALTER TABLE "Todo" ALTER COLUMN "userId" SET NOT NULL;

-- AddForeignKey
ALTER TABLE "Todo" ADD CONSTRAINT "Todo_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE;