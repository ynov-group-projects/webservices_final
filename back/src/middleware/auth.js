const jwt = require('jsonwebtoken');
const {AuthenticationError} = require("apollo-server-express");

const auth = (req) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split('Bearer ')[1];
        if (token) {
            try {
                const user = jwt.verify(token, process.env.JWT_SECRET);
                return user;
            } catch (e) {
                throw new AuthenticationError('Invalid/Expired token');
            }
        }
    }
    return null;
};

module.exports = auth;