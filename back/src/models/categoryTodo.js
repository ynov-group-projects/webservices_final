const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

class CategoryTodo {

    constructor({ id, name, description, userId, color }) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.userId = userId;
        this.color = color;
    }

    static async create(userId, { name, description, color }) {
        const category = await prisma.categoryTodo.create({
            data: { name, description, userId, color }
        });
        return new CategoryTodo(category);
    }

    static async findById(id) {
        const category = await prisma.categoryTodo.findUnique({
            where: { id: parseInt(id) },
            include: { todos: true }
        });
        return category ? new CategoryTodo(category) : null;
    }

    static async update(id, userId, { name, description, color }) {
        const category = await prisma.categoryTodo.findUnique({
            where: { id: parseInt(id) }
        });

        if (!category) {
            throw new Error('NOT_FOUND');
        }

        if (category.userId !== userId) {
            throw new Error('FORBIDDEN');
        }

        const updatedCategory = await prisma.categoryTodo.update({
            where: { id: parseInt(id) },
            data: { name, description, color }
        });
        return new CategoryTodo(updatedCategory);
    }

    static async delete(id, userId) {
        const category = await prisma.categoryTodo.findUnique({
            where: { id: parseInt(id) }
        });

        if (!category) {
            throw new Error('NOT_FOUND');
        }

        if (category.userId !== userId) {
            throw new Error('FORBIDDEN');
        }

        // Delete associated todos first
        await prisma.todo.deleteMany({
            where: { categoryId: parseInt(id) }
        });

        // Then delete the category
        await prisma.categoryTodo.delete({
            where: { id: parseInt(id) }
        });

        return true;
    }
}

module.exports = CategoryTodo;