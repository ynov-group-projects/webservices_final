const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

class User {
    constructor({ id, name, email, password }) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    static async findById(id) {
        const user = await prisma.user.findUnique({
            where: { id: parseInt(id) },
            include: { categories: { include: { todos: true } } }
        });
        return user ? new User(user) : null;
    }

    static async findOne({ email }) {
        const user = await prisma.user.findUnique({ where: { email } });
        return user ? new User(user) : null;
    }

    async save() {
        const { name, email, password } = this;
        try {
            const user = await prisma.user.create({
                data: { name, email, password },
            });
            return new User(user);
        } catch (error) {
            if (error.code === 'P2002') {
                throw new Error('User already exists');
            }
            throw error;
        }
    }

    async getCategories() {
        return await prisma.categoryTodo.findMany({
            where: { userId: this.id },
            include: { todos: true },
            orderBy: {
                createdAt: 'desc'
            }
        });
    }
}

module.exports = User;