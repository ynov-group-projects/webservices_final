const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

class Todo {
    constructor({ id, title, description, dueDate, userId, categoryId, priority, status }) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.userId = userId;
        this.categoryId = categoryId;
        this.priority = priority;
        this.status = status;
    }

    static async create({ categoryId, title, description, dueDate, userId, priority, status }) {
        const todo = await prisma.todo.create({
            data: {
                title,
                description,
                dueDate,
                userId: parseInt(userId),
                categoryId: parseInt(categoryId),
                priority,
                status
            }
        });
        return new Todo(todo);
    }

    static async update({ id, title, description, dueDate, priority, status }) {
        const todo = await prisma.todo.update({
            where: { id: parseInt(id) },
            data: { title, description, dueDate, priority, status }
        });
        return new Todo(todo);
    }

    static async delete(id, userId) {
        const todo = await prisma.todo.findUnique({
            where: { id: parseInt(id) },
            include: { category: { select: { userId: true } } }
        });

        if (!todo) {
            throw new Error('Todo not found');
        }

        if (todo.category.userId !== userId) {
            throw new Error('Not authorized to delete this todo');
        }

        await prisma.todo.delete({ where: { id: parseInt(id) } });
        return true;
    }

    static async findById(id) {
        const todo = await prisma.todo.findUnique({
            where: { id: parseInt(id) },
            include: { category: { select: { userId: true } } }
        });
        if (!todo) {
            throw new Error('Todo not found');
        }
        return new Todo(todo);
    }
}

module.exports = Todo;