const { gql } = require('apollo-server-express');

const categoryTodoSchema = gql`
    type Query {
        myCategories: [CategoryTodo!]!
    }

    type CategoryTodo {
        id: ID!
        name: String!
        description: String
        color: String!
        todos: [Todo!]!
    }

    type Mutation {
        createCategory(name: String!, description: String, color: String!): CategoryTodo
        updateCategory(id: ID!, name: String, description: String, color: String): CategoryTodo
        deleteCategory(id: ID!): Boolean
    }
`;

module.exports = categoryTodoSchema;