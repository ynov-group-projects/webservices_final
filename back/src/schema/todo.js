const { gql } = require('apollo-server-express');

const todoSchema = gql`
    enum TodoPriorityEnum {
        LOW
        MEDIUM
        HIGH
    }

    enum TodoStatusEnum {
        TO_START
        IN_PROGRESS
        COMPLETED
    }

    type Todo {
        id: ID!
        title: String!
        description: String
        dueDate: String
        userId: ID!
        categoryId: ID!
        priority: TodoPriorityEnum!
        status: TodoStatusEnum!
    }

    type Mutation {
        createTodo(categoryId: ID!, title: String!, description: String, dueDate: String, priority: TodoPriorityEnum!, status: TodoStatusEnum!): Todo
        updateTodo(id: ID!, title: String, description: String, dueDate: String, priority: TodoPriorityEnum, status: TodoStatusEnum): Todo
        deleteTodo(id: ID!): Boolean
    }
`;

module.exports = todoSchema;