const CategoryTodo = require('../models/categoryTodo');
const Todo = require('../models/todo');
const { ApolloError, UserInputError, AuthenticationError, ForbiddenError } = require('apollo-server-express');

const resolvers = {
    Mutation: {
        createTodo: async (_, { categoryId, title, description, dueDate, priority, status }, context) => {
            if (!context.user) throw new Error('Not authenticated');
            const category = await CategoryTodo.findById(categoryId);
            if (!category || category.userId !== context.user.userId) {
                throw new ForbiddenError('Category not found or not owned by user');
            }
            return await Todo.create({ categoryId, title, description, dueDate, userId: context.user.userId, priority, status });
        },

        updateTodo: async (_, { id, title, description, dueDate, priority, status }, context) => {
            if (!context.user) throw new AuthenticationError('Not authenticated');
            try {
                const todo = await Todo.findById(id);
                if (!todo) {
                    throw new ForbiddenError('Todo not found');
                }
                if (todo.userId !== context.user.userId) {
                    throw new ForbiddenError('Not authorized to update this todo');
                }
                return await Todo.update({ id, title, description, dueDate, priority, status });
            } catch (error) {
                if (error instanceof UserInputError || error instanceof ForbiddenError) {
                    throw error;
                }
                console.error('Error updating todo:', error);
                throw new Error('An error occurred while updating the todo');
            }
        },

        deleteTodo: async (_, { id }, context) => {
            if (!context.user) throw new AuthenticationError('Not authenticated');
            try {
                const result = await Todo.delete(id, context.user.userId);
                return result;
            } catch (error) {
                if (error.message === 'Todo not found') {
                    throw new ForbiddenError('Todo not found');
                }
                if (error.message === 'Not authorized to delete this todo') {
                    throw new ForbiddenError('Not authorized to delete this todo');
                }
                console.error('Error deleting todo:', error);
                throw new ApolloError('An error occurred while deleting the todo');
            }
        },
    },
};

module.exports = resolvers;