const User = require('../models/user');
const CategoryTodo = require('../models/categoryTodo');
const { ApolloError, UserInputError, AuthenticationError, ForbiddenError } = require('apollo-server-express');

const resolvers = {
    Query: {
        myCategories: async (_, __, context) => {
            if (!context.user) throw new Error('Not authenticated');
            const user = await User.findById(context.user.userId);
            return user.getCategories();
        },
    },
    Mutation: {
        createCategory: async (_, { name, description, color }, context) => {
            if (!context.user) throw new Error('Not authenticated');
            return await CategoryTodo.create(context.user.userId, { name, description, color });
        },

        updateCategory: async (_, { id, name, description, color }, context) => {
            if (!context.user) throw new AuthenticationError('Not authenticated');
            try {
                return await CategoryTodo.update(id, context.user.userId, {name, description, color});
            } catch (error) {
                console.error('Error updating category:', error);
                if (error.message === 'NOT_FOUND') {
                    throw new ForbiddenError('Category not found', { code: 'NOT_FOUND' });
                }
                if (error.message === 'FORBIDDEN') {
                    throw new ForbiddenError('Not authorized to update this category');
                }
                throw new ApolloError('An error occurred while updating the category');
            }
        },

        deleteCategory: async (_, { id }, context) => {
            if (!context.user) throw new AuthenticationError('Not authenticated');
            try {
                const result = await CategoryTodo.delete(id, context.user.userId);
                return result;
            } catch (error) {
                console.error('Error deleting category:', error);
                if (error.message === 'NOT_FOUND') {
                    throw new ForbiddenError('Category not found');
                }
                if (error.message === 'FORBIDDEN') {
                    throw new ForbiddenError('Not authorized to delete this category');
                }
                throw new ApolloError('An error occurred while deleting the category');
            }
        }
    },
};

module.exports = resolvers;