require('dotenv').config();
const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const userSchema = require('./src/schema/user');
const categoryTodoSchema = require('./src/schema/categoryTodo');
const todoSchema = require('./src/schema/todo');
const userResolvers = require('./src/resolvers/user');
const categoryTodoResolvers = require('./src/resolvers/categoryTodo');
const todoResolvers = require('./src/resolvers/todo');
const auth = require('./src/middleware/auth');
const cors = require('cors')

const app = express();
app.use(cors())

app.get('/status', function (req, res) {
    res.json({ status: 'UP'});
});

const server = new ApolloServer({
    typeDefs: [userSchema, categoryTodoSchema, todoSchema],
    resolvers: [userResolvers, categoryTodoResolvers, todoResolvers],
    context: ({ req }) => {
        const user = auth(req);
        return { user };
    },
    formatError: (err) => {
        if (err.extensions.code === 'UNAUTHENTICATED') {
            return {
                message: err.message,
                status: 401,
                code: 'UNAUTHENTICATED'
            };
        }
        if (err.extensions.code === 'FORBIDDEN') {
            return {
                message: err.message,
                status: 403,
                code: 'FORBIDDEN'
            };
        }
        if (err.extensions.code === 'NOT_FOUND') {
            return {
                message: err.message,
                status: 404,
                code: 'NOT_FOUND'
            };
        }
        return {
            message: err.message,
            status: 500,
            code: 'INTERNAL_SERVER_ERROR'
        };
    },
});

async function startServer() {
    await server.start();
    server.applyMiddleware({ app });

    const PORT = process.env.PORT || 4000;
    app.listen(PORT, () => {
        console.log(`Server running on http://localhost:${PORT}${server.graphqlPath}`);
    });
}

startServer();