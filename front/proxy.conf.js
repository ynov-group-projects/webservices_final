const PROXY_SERVER = 'http://localhost:4000';

const PROXY_CONFIG = [
  {
    context: '/',
    target: PROXY_SERVER,
    secure: false,
  }
]

module.exports = PROXY_CONFIG;
