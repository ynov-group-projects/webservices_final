/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  safelist: [
    "bg-error-300",
    "bg-warning-300",
    "bg-success-300",
    "bg-info-300",

    "bg-TO_START",
    "bg-IN_PROGRESS",
    "bg-COMPLETED",

    "text-error-700",
    "text-warning-700",
    "text-success-700",
    "text-info-700",

    "text-LOW",
    "text-MEDIUM",
    "text-HIGH",

    "border-error-700",
    "border-warning-700",
    "border-success-700",
    "border-info-700",
  ],
  theme: {
    extend: {
      fontFamily: {
        'montserrat': ['Montserrat', 'sans-serif'],
      },
      colors: {
        primary: '#07B5C5',
        secondary: '#333745',
        contrast: '#f6fffd',
        danger: '#fd7171',

        "error-300": "#FCA5A5",
        "error-700": "#B91C1C",
        "warning-300": "#FDE68A",
        "warning-700": "#D97706",
        "success-300": "#A7F3D0",
        "success-700": "#059669",
        "info-300": "#BFDBFE",
        "info-700": "#2563EB",

        "TO_START": "#b4b4b4",
        "IN_PROGRESS": "#145b98",
        "COMPLETED": "#4e8d17",

        "LOW": "#3c9120",
        "MEDIUM": "#e0bf1b",
        "HIGH": "#c92323",
      },
      spacing: {
        'full-100px': 'calc(100vh - 160px)',
      }
    },
  },
  plugins: [],
}

