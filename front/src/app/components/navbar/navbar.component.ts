import {Component, inject} from '@angular/core';
import {RouterLink} from "@angular/router";
import {AuthService} from "../../security/auth.service";
import {ConfirmationDialogComponent} from "../confirmation-dialog/confirmation-dialog.component";
import {UserModel} from "../../models/user.model";
import {MatDialog} from "@angular/material/dialog";
import {CategoryDialogComponent} from "../../features/todos-page/category-dialog/category-dialog.component";
import {CategoryService} from "../../services/category.service";
import {TodoCategoryModel} from "../../models/todo-category.model";
import {MutationResult} from "apollo-angular/types";
import {ToastService} from "../../services/toast.service";
import {AnimationService} from "../../services/animation.service";

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    RouterLink
  ],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {
  authService = inject(AuthService);
  dialog = inject(MatDialog);
  categoryService = inject(CategoryService);
  toasterService = inject(ToastService);
  animationService = inject(AnimationService);

  logout() {
    this.authService.logout();
  }

  createCategory() {
    this.dialog.open(CategoryDialogComponent, {
      width: '512px',
      enterAnimationDuration: '200ms',
      exitAnimationDuration: '100ms',
      data: {
        action: 'create'
      }
    }).afterClosed().subscribe(result => {
      this.categoryService.createCategory(result)
        .subscribe((result: MutationResult<{createCategory: TodoCategoryModel}>) => {
          if(result.data) {
            this.categoryService.categorySubject.next([result.data.createCategory, ...this.categoryService.categorySubject.getValue()]);
            this.toasterService.addToast("Category created successfully!",'success');
            setTimeout(() => {
              this.animationService.getTodosPageComponentInstance().initScrollAnimation();
            }, 1000);
          } else {
            this.toasterService.addToast("Failed to create category!",'error');
          }
        });
    });
  }
}
