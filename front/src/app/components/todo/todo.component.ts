import {Component, inject, Input} from '@angular/core';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import {TodoStatusType} from "../../types/todo-status.type";
import {TodoModel} from "../../models/todo.model";
import {MatDialog} from "@angular/material/dialog";
import {TodoDialogComponent} from "../../features/todos-page/todo-dialog/todo-dialog.component";
import {CategoryService} from "../../services/category.service";
import {TodoService} from "../../services/todo.service";
import {ToastService} from "../../services/toast.service";
import {DatePipe, NgClass, NgForOf, NgIf, NgStyle} from "@angular/common";

type TodoPriorityType = 'LOW' | 'MEDIUM' | 'HIGH';

@Component({
  selector: 'app-todo',
  standalone: true,
  imports: [MatFormFieldModule, MatSelectModule, MatInputModule, NgStyle, NgClass, NgForOf, NgIf, DatePipe],
  templateUrl: './todo.component.html',
  styleUrl: './todo.component.scss'
})
export class TodoComponent {
  categoryService = inject(CategoryService);
  todoService = inject(TodoService);
  dialog = inject(MatDialog);
  toasterService = inject(ToastService);

  status: TodoStatusType[] = ['TO_START', 'IN_PROGRESS', 'COMPLETED'];
  priority = ['LOW', 'MEDIUM', 'HIGH'];
  state = false
  @Input() todo!: TodoModel;

  changeState() {
    this.state = !this.state
  }

  timeUntil(timestamp: Date) {

  }

  updateTodo(): void {
    this.dialog.open(TodoDialogComponent, {
      width: '512px',
      enterAnimationDuration: '200ms',
      exitAnimationDuration: '100ms',
      data: {
        action: 'update',
        todo: this.todo,
        id: this.todo.id
      }
    }).afterClosed().subscribe(result => {
      this.todoService.updateTodo(result).subscribe((todo) => {
          this.categoryService.updateTodoInCategoryStore(result.categoryId, result);
          this.toasterService.addToast("Todo updated successfully!",'success');
        }
      );
    });
  }

  updateStatus(status: TodoStatusType): void {
    const todo = {...this.todo, status: status, dueDate: new Date()};
    this.todoService.updateTodoStatus(todo).subscribe((todo) => {
        // @ts-ignore
      this.categoryService.updateTodoInCategoryStore(todo.data.updateTodo.categoryId, todo);
      this.toasterService.addToast("Todo status updated successfully!",'success');
      }
    );
  }

  nextPriority(): void {
    let nextElement = (this.priority.indexOf(this.todo.priority) + 1 < this.priority.length) ? this.priority.indexOf(this.todo.priority) + 1 : 0;
    const todo = {...this.todo, dueDate: new Date(), priority: this.priority[nextElement] as TodoPriorityType};
    this.todoService.updateTodoPriority(todo).subscribe((todo) => {
        // @ts-ignore
      this.categoryService.updateTodoInCategoryStore(todo.data.updateTodo.categoryId, todo);
        this.toasterService.addToast("Todo priority updated successfully!",'success');
      }
    );
  }
}
