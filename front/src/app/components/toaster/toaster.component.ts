import { Component } from '@angular/core';
import {NgClass, NgForOf} from "@angular/common";
import { ToastService } from '../../services/toast.service';

export type ToastType = 'info' | 'success' | 'error' | 'warning';

type Toast = {
  id: number;
  message: string;
  type: ToastType;
};

@Component({
  selector: 'app-toaster',
  standalone: true,
  imports: [
    NgForOf,
    NgClass
  ],
  templateUrl: './toaster.component.html',
  styleUrl: './toaster.component.scss'
})
export class ToasterComponent {
  toastList: Toast[] = [];

  constructor(private toastService: ToastService) {}

  ngOnInit() {
    this.toastService.setToaster(this);
  }

  removeToast(id: number) {
    this.toastList = this.toastList.filter((toast) => toast.id !== id);
  }
}
