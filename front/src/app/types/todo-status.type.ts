export type TodoStatusType = 'TO_START' | 'IN_PROGRESS' | 'COMPLETED';
