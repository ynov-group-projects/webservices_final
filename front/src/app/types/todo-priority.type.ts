export type TodoPriorityType = 'LOW' | 'MEDIUM' | 'HIGH';
