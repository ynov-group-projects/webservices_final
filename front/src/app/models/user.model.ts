import {BaseEntityModel} from "./base-entity.model";

export interface UserModel extends BaseEntityModel {
  id: number;
  name: string;
  email: string;
  password: string;
}
