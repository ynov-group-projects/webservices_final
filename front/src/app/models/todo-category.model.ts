import {BaseEntityModel} from "./base-entity.model";
import {TodoModel} from "./todo.model";

export interface TodoCategoryModel extends BaseEntityModel {
  id: number;
  name: string;
  description: string;
  color: string;
  todos: TodoModel[];
}
