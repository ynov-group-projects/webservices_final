export interface BaseEntityModel {
  createdAt: Date;
  updatedAt: Date;
}
