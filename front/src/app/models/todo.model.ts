import {BaseEntityModel} from "./base-entity.model";
import {TodoPriorityType} from "../types/todo-priority.type";
import {TodoStatusType} from "../types/todo-status.type";

export interface TodoModel extends BaseEntityModel {
  id: number;
  title: string;
  description: string;
  dueDate: number;
  priority: TodoPriorityType
  status: TodoStatusType
  categoryId: number;
}
