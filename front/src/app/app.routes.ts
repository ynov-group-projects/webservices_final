import {Routes} from '@angular/router';
import {TodosPageComponent} from "./features/todos-page/todos-page.component";
import {NotFoundPageComponent} from "./features/not-found-page/not-found-page.component";
import {LoginPageComponent} from "./features/login-page/login-page.component";
import {authGuard} from "./security/auth-guard";

export const routes: Routes = [
  //Public routes
  {
    component: TodosPageComponent,
    path: "",
    canActivate: [authGuard]
  },
  {
    component: LoginPageComponent,
    path: "login"
  },
  //Wildcard route for 404
  {
    component: NotFoundPageComponent,
    path: "**",
  }
];
