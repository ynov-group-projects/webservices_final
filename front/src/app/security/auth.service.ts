import {inject, Injectable} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Apollo, gql} from "apollo-angular";
import {UserModel} from "../models/user.model";
import {Observable, OperatorFunction, tap} from "rxjs";
import type {MutationResult} from "apollo-angular/types";
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";
import {ToastService} from "../services/toast.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  dialog = inject(MatDialog);
  apollo = inject(Apollo);
  router = inject(Router);
  userService = inject(UserService);
  toasterService = inject(ToastService);

  constructor() { }

  authenticate(user: UserModel): Observable<any> {
    return this.apollo.mutate<LoginResponse>({
      mutation: gql`
      mutation login($email: String!, $password: String!) {
        login(email: $email, password: $password)
      }
    `,
      variables: {
        email: user.email,
        password: user.password
      }
    }).pipe(
      tap((res) => {
        if(res.data?.login) {
          localStorage.setItem('token', res.data?.login);
          this.userService.token.set(this.getToken());
          this.router.navigate(['/']).then();
          this.toasterService.addToast("You logged in with success!",'success');
        } else {
          this.toasterService.addToast("You had an error while login into your account, try again!",'error');
        }
      })
    )
  }

  register(user: UserModel): Observable<MutationResult<UserModel>>{
    return this.apollo.mutate({
      mutation: gql`
        mutation register($name: String!, $password: String!, $email: String!) {
          register(name: $name, password: $password, email: $email) {
            id
            name
            email
          }
        }
      `,
      variables: {
        name: user.name,
        email: user.email,
        password: user.password
      }
    })
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  isLoggedIn(): boolean {
    return !!this.getToken();
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']).then();
    this.toasterService.addToast("You logged off with success!",'success');
  }
}

interface LoginResponse {
  login: string;
}
