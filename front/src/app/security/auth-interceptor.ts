import {inject} from '@angular/core';
import {HttpErrorResponse, HttpInterceptorFn,} from '@angular/common/http';
import {AuthService} from "./auth.service";
import {catchError, throwError} from "rxjs";

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  let authService = inject(AuthService);

  if(!authService.isLoggedIn()) return next(req);

  const modifiedRequest = req.clone({
    setHeaders: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS',
      'Access-Control-Allow-Headers': 'Origin,Content-Type,Accept',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${authService.getToken()}`
    },
  });

  return next(modifiedRequest).pipe(
    catchError((error: HttpErrorResponse) => {
      if (error.error.errors[0].status == 401) {
        authService.logout();
      }
      return throwError(() => error);
    })
  );
};
