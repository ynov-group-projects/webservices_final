import {Component, inject, OnInit} from '@angular/core';
import {Router, RouterLink} from "@angular/router";
import {NgClass, NgIf} from "@angular/common";
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  Validators
} from "@angular/forms";
import {AuthService} from "../../security/auth.service";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmationDialogComponent} from "../../components/confirmation-dialog/confirmation-dialog.component";
import {UserModel} from "../../models/user.model";
import {ToastService} from "../../services/toast.service";

@Component({
  selector: 'app-login-page',
  standalone: true,
  imports: [
    RouterLink,
    NgClass,
    ReactiveFormsModule,
    NgIf
  ],
  templateUrl: './login-page.component.html',
  styleUrl: './login-page.component.scss'
})
export class LoginPageComponent implements OnInit{
  authService = inject(AuthService);
  dialog = inject(MatDialog);
  router = inject(Router);
  toasterService = inject(ToastService);

  isLogin = true;
  registerForm!: FormGroup;

  loginForm!: FormGroup;


  ngOnInit(): void {
    this.registerForm = new FormGroup({
      nameRegister: new FormControl('', [Validators.required, Validators.minLength(3)]),
      emailRegister: new FormControl('', [Validators.required, Validators.email]),
      passwordRegister: new FormControl('', [Validators.required, Validators.minLength(8)]),
      repeatPasswordRegister: new FormControl('', [Validators.required])
    }, { validators: this.passwordMatchValidator });

    this.loginForm = new FormGroup({
      emailLogin: new FormControl('', [Validators.required]),
      passwordLogin: new FormControl('', [Validators.required])
    });
  }

  switchMode() {
    this.isLogin = !this.isLogin;
  }

  onSubmitRegister() {
    this.dialog.open(ConfirmationDialogComponent, {
      width: '512px',
      enterAnimationDuration: '200ms',
      exitAnimationDuration: '100ms',
      data: {
        title: 'Register',
        message: 'Are you sure you want to register?'
      }
    }).afterClosed().subscribe(result => {
      if (result) {
        if(this.registerForm.valid) {
          this.authService.register({
            name: this.registerForm.get('nameRegister')!.value,
            email: this.registerForm.get('emailRegister')!.value,
            password: this.registerForm.get('passwordRegister')!.value
          } as UserModel).subscribe();
          this.registerForm.reset();
          this.isLogin = true;
          this.toasterService.addToast("You created your account with success, you can now login!",'success');
        }
      } else {
        this.toasterService.addToast("You had an error while registering your account, try again!",'error');
      }
    });
  }

  onSubmitLogin() {
    if(this.loginForm.valid) {
      this.authService.authenticate({
        email: this.loginForm.get('emailLogin')!.value,
        password: this.loginForm.get('passwordLogin')!.value
      } as UserModel).subscribe();
    }
    this.loginForm.reset();
  }


  passwordMatchValidator(control: AbstractControl): ValidationErrors | null {
    const password = control.get('passwordRegister');
    const repeatPassword = control.get('repeatPasswordRegister');

    if (password && repeatPassword && password.value !== repeatPassword.value) {
      return { passwordMismatch: true };
    }
    return null;
  }

}
