import {Component, inject, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogClose, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {NgIf} from "@angular/common";
import {DialogActions} from "../../../types/actions.types";
import {TodoModel} from "../../../models/todo.model";
import {CategoryService} from "../../../services/category.service";
import {TodoService} from "../../../services/todo.service";
import {ToastService} from "../../../services/toast.service";

export interface TodoDialogData {
  action: DialogActions;
  id?: number;
  todo?: TodoModel;
}

@Component({
  selector: 'app-todo-dialog',
  standalone: true,
  imports: [
    MatDialogClose,
    ReactiveFormsModule,
    NgIf
  ],
  templateUrl: './todo-dialog.component.html',
  styleUrl: './todo-dialog.component.scss'
})
export class TodoDialogComponent implements OnInit {
  todoForm!: FormGroup;
  todoService = inject(TodoService);
  categoryService = inject(CategoryService);
  toastService = inject(ToastService);

  constructor(
    public dialogRef: MatDialogRef<TodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TodoDialogData
  ) {}

  ngOnInit(): void {
    this.todoForm = new FormGroup({
      title: new FormControl(this.data.todo?.title ?? '', [Validators.required, Validators.minLength(3)]),
      description: new FormControl(this.data.todo?.description ?? '', [Validators.required]),
      dueDate: new FormControl(this.data.todo?.dueDate ?? ''),
      priority: new FormControl(this.data.todo?.priority ?? 'LOW'),
      status: new FormControl(this.data.todo?.status ?? 'TO_START')
    });
  }

  onConfirm(): void {
    this.dialogRef.close({
      id: (this.data.id) ? this.data.id : null,
      ...this.todoForm.value}
    );
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

  onDelete(): void {
    if(this.data.id && this.data.todo) {
      this.todoService.deleteTodo(this.data.id, this.data.todo).subscribe(() => {
        if(this.data.todo) {
          this.categoryService.removeTodoFromCategory(this.data.todo.categoryId, this.data.todo.id);
          this.toastService.addToast("Todo deleted successfully!", 'success');
        } else {
          this.toastService.addToast("Todo was not deleted successfully!", 'error');
        }
      })
      this.onConfirm();
    }
    return;
  }
}
