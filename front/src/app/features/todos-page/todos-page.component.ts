import { AfterViewInit, Component, inject, OnInit } from '@angular/core';
import { NavbarComponent } from "../../components/navbar/navbar.component";
import { TodoComponent } from "../../components/todo/todo.component";
import { NgForOf, NgStyle } from "@angular/common";
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { CategoryService } from "../../services/category.service";
import { MatDialog } from "@angular/material/dialog";
import { TodoDialogComponent } from "./todo-dialog/todo-dialog.component";
import { TodoService } from "../../services/todo.service";
import { CategoryDialogComponent } from "./category-dialog/category-dialog.component";
import { ToastService } from "../../services/toast.service";
import { TodoCategoryModel } from "../../models/todo-category.model";
import { AnimationService } from "../../services/animation.service";

gsap.registerPlugin(ScrollTrigger);

@Component({
  selector: 'app-todos-page',
  standalone: true,
  imports: [
    NavbarComponent,
    TodoComponent,
    NgForOf,
    NgStyle
  ],
  templateUrl: './todos-page.component.html',
  styleUrls: ['./todos-page.component.scss']
})
export class TodosPageComponent implements AfterViewInit, OnInit {
  categoryServices = inject(CategoryService);
  dialog = inject(MatDialog);
  categoryService = inject(CategoryService);
  todoService = inject(TodoService);
  toasterService = inject(ToastService);
  animateService = inject(AnimationService);

  ngOnInit(): void {
    this.categoryServices.getMyCategories().subscribe();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.initScrollAnimation();
      this.animateService.setTodosPageComponentInstance(this);
    }, 1000);
  }

  createTodo(categoryId: number) {
    this.dialog.open(TodoDialogComponent, {
      width: '512px',
      enterAnimationDuration: '200ms',
      exitAnimationDuration: '100ms',
      data: {
        action: 'create'
      }
    }).afterClosed().subscribe(result => {
      this.todoService.createTodo(categoryId, result).subscribe((todo) => {
        this.categoryService.addTodoToCategoryStore(categoryId, todo.data!.createTodo);
        this.toasterService.addToast("Todo was created successfully!",'success');
      });
    });
  }

  updateCategory(category: TodoCategoryModel) {
    this.dialog.open(CategoryDialogComponent, {
      width: '512px',
      enterAnimationDuration: '200ms',
      exitAnimationDuration: '100ms',
      data: {
        action: 'update',
        category: category,
        id: category.id
      }
    }).afterClosed().subscribe(result => {
      this.categoryService.updateCategory(result).subscribe((todo) => {
          this.categoryService.updateCategory(result);
          this.toasterService.addToast("Category was updated successfully!",'success');
        }
      );
    });
  }

  initScrollAnimation(): void {
    console.log('initScrollAnimation')
    ScrollTrigger.getAll().forEach(trigger => trigger.kill());

    let listItems = document.querySelectorAll('.scroll-item')
    let totalWidth = 0;
    listItems.forEach((item) => totalWidth += item.clientWidth)
    let pxToPercent = (px: number) => (px / window.innerWidth * 100);
    let withToPercent = (totalWidth / window.innerWidth * 100) - 100 + pxToPercent(27 * listItems.length) + pxToPercent(80);
    if(withToPercent < 0) return
    gsap.to('.horizontal-scroll-container', {
      xPercent: -withToPercent,
      ease: 'none',
      scrollTrigger: {
        trigger: '.horizontal-scroll-container',
        pin: true,
        scrub: 1,
        end: () => `+=${document.querySelector('.horizontal-scroll-container')?.scrollWidth}`
      }
    });
  }
}
