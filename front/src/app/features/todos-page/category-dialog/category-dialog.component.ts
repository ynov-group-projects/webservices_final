import {Component, inject, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogClose, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {NgIf} from "@angular/common";
import {DialogActions} from "../../../types/actions.types";
import {TodoCategoryModel} from "../../../models/todo-category.model";
import {CategoryService} from "../../../services/category.service";
import {ToastService} from "../../../services/toast.service";
import {AnimationService} from "../../../services/animation.service";

export interface CategoryDialogData {
  action: DialogActions;
  id?: number;
  category?: TodoCategoryModel;
}

@Component({
  selector: 'app-category-dialog',
  standalone: true,
  imports: [
    MatDialogClose,
    ReactiveFormsModule,
    NgIf
  ],
  templateUrl: './category-dialog.component.html',
  styleUrl: './category-dialog.component.scss'
})
export class CategoryDialogComponent implements OnInit {
  categoryForm!: FormGroup;
  categoryService = inject(CategoryService);
  toasterService = inject(ToastService);
  animationService = inject(AnimationService);

  constructor(
    public dialogRef: MatDialogRef<CategoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CategoryDialogData
  ) {}

  ngOnInit(): void {
    this.categoryForm = new FormGroup({
      name: new FormControl(this.data.category?.name ?? '', [Validators.required, Validators.minLength(3)]),
      color: new FormControl(this.data.category?.color ?? '#000000', [Validators.required]),
    });
  }

  onConfirm(): void {
    this.dialogRef.close({
      id: (this.data.id) ? this.data.id : null,
      ...this.categoryForm.value}
    );
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

  onDelete(): void {
    if(this.data.id) {
      this.categoryService.deleteCategory(this.data.id).subscribe()
      this.toasterService.addToast("Category was deleted successfully!", 'success');
      this.onConfirm();
      setTimeout(() => {
        this.animationService.getTodosPageComponentInstance().initScrollAnimation();
      }, 1000);
    }
    return;
  }
}
