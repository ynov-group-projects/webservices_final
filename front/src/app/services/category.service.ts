import {inject, Injectable} from '@angular/core';
import {Apollo, gql} from "apollo-angular";
import {BehaviorSubject, map, Observable, take} from "rxjs";
import {TodoCategoryModel} from "../models/todo-category.model";
import {MutationResult} from "apollo-angular/types";
import {QueryResult} from "@apollo/client";
import {TodoModel} from "../models/todo.model";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  apollo = inject(Apollo);
  categorySubject = new BehaviorSubject<TodoCategoryModel[]>([]);

  constructor() { }

  createCategory(todoCategoryModel: TodoCategoryModel): Observable<MutationResult<{createCategory: TodoCategoryModel}>> {
    return this.apollo.mutate<{createCategory: TodoCategoryModel}>({
      mutation: gql`
        mutation createCategory($name: String!, $color: String!) {
          createCategory(name: $name, color: $color) {
            id
            name
            color
          }
        }
      `,
        variables: {
          name: todoCategoryModel.name,
          color: todoCategoryModel.color
        }
      }
    )
  }

  getMyCategories(): Observable<TodoCategoryModel[]> {
    return this.apollo.watchQuery<{ myCategories: TodoCategoryModel[] }>({
      query: gql`
        query {
          myCategories {
            id
            name
            description
            color
            todos {
              id
              title
              description
              dueDate
              priority
              status
            }
          }
        }
      `
    })
      .valueChanges
      .pipe(
        map(result => {
          this.categorySubject.next(result.data.myCategories);
          return result.data.myCategories;
        })
      );
  }

  updateCategory(todoCategoryModel: TodoCategoryModel): Observable<MutationResult<{updateCategory: TodoCategoryModel}>> {
    return this.apollo.mutate<{updateCategory: TodoCategoryModel}>({
      mutation: gql`
        mutation updateCategory($id: ID!, $name: String!, $color: String!) {
          updateCategory(id: $id, name: $name, color: $color) {
            id
            name
            color
          }
        }
      `,
        variables: {
          id: todoCategoryModel.id,
          name: todoCategoryModel.name,
          color: todoCategoryModel.color
        }
      }
    );
  }

  deleteCategory(categoryId: number): Observable<MutationResult<{createCategory: TodoCategoryModel}>> {
    return this.apollo.mutate<{createCategory: TodoCategoryModel}>({
        mutation: gql`
        mutation deleteCategory($id: ID!) { deleteCategory(id: $id) }
      `,
        variables: {
          id: categoryId
        }
      }
    ).pipe(
      map(result => {
        this.removeCategoryByIdStore(categoryId);
        return result;
      })
    );
  }

  addTodoToCategoryStore(categoryId: number, todo: TodoModel): void {
    const currentCategories = this.categorySubject.getValue();
    const updatedCategories = currentCategories.map(category => {
      if(!category.todos) category.todos = [];
      if (category.id === categoryId) {
        return {
          ...category,
          todos: [...category.todos, todo]
        };
      }
      return category;
    });

    this.categorySubject.next(updatedCategories);
  }

  updateTodoInCategoryStore(categoryId: number, updatedTodo: TodoModel): void {
    const currentCategories = this.categorySubject.getValue();
    const updatedCategories = currentCategories.map(category => {
      if (category.id === categoryId) {
        const updatedTodos = category.todos.map(todo => {
          if (todo.id === updatedTodo.id) {
            return { ...todo, ...updatedTodo };
          }
          return todo;
        });
        return {
          ...category,
          todos: updatedTodos
        };
      }
      return category;
    });

    this.categorySubject.next(updatedCategories);
  }

  removeCategoryByIdStore(categoryId: number): void {
    const currentCategories = this.categorySubject.getValue();
    const updatedCategories = currentCategories.filter(category => category.id !== categoryId);

    this.categorySubject.next(updatedCategories);
  }

  removeTodoFromCategory(categoryId: number, todoId: number): void {
    const currentCategories = this.categorySubject.getValue();

    const updatedCategories = currentCategories.map(category => {
      if (category.id === categoryId) {
        const updatedTodos = category.todos.filter(todo => todo.id !== todoId);
        return {
          ...category,
          todos: updatedTodos
        };
      }
      return category;
    });

    this.categorySubject.next(updatedCategories);
  }
}
