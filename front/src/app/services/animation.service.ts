import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnimationService {
  private todosPageComponentInstance: any;

  setTodosPageComponentInstance(instance: any) {
      this.todosPageComponentInstance = instance;
  }

  getTodosPageComponentInstance() {
    return this.todosPageComponentInstance;
  }
}
