import {Injectable, signal, WritableSignal} from '@angular/core';
import {UserModel} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  token: WritableSignal<string | null> = signal(null);

  getUser(): UserModel | null {
    if(this.token()) {
      const decodedToken = this.decodeToken(this.token());
      return {
        id: Number(decodedToken?.id),
        name: String(decodedToken?.name),
        email: String(decodedToken?.email)
      } as UserModel;
    }
    return null;
  }


  decodeToken(token: string | null): any {
    try {
      if(token) return JSON.parse(atob(token.split('.')[1]));
    } catch (error) {
      console.error('Error decoding token:', error);
      return null;
    }
  }
}
