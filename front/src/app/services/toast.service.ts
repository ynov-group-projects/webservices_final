import { Injectable } from '@angular/core';
import {ToasterComponent, ToastType} from "../components/toaster/toaster.component";

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  private toaster: ToasterComponent | null = null;

  constructor() {}

  setToaster(toaster: ToasterComponent) {
    this.toaster = toaster;
  }

  addToast(message: string, type: ToastType = 'info') {
    if (this.toaster) {
      this.toaster.toastList.push({
        id: Math.random(),
        message: message,
        type: type
      });
      setTimeout(() => {
        if  (this.toaster && this.toaster.toastList[0]) {
          this.toaster.removeToast(this.toaster.toastList[0].id);
        }
      }, 3000);
    }
  }
}
