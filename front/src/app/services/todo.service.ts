import {inject, Injectable} from '@angular/core';
import {Apollo, gql} from "apollo-angular";
import {CategoryService} from "./category.service";
import {TodoModel} from "../models/todo.model";
import {map, Observable} from "rxjs";
import {TodoCategoryModel} from "../models/todo-category.model";
import {MutationResult} from "apollo-angular/types";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  apollo = inject(Apollo);
  categoryService = inject(CategoryService);

  constructor() { }

  createTodo(categoryId: number, todo: TodoModel): Observable<MutationResult<{createTodo: TodoModel}>> {
    const isoDate = todo.dueDate ? new Date(todo.dueDate).toISOString() : null;
    return this.apollo.mutate<{createTodo: TodoModel}>({
        mutation: gql`
          mutation createTodo($categoryId: ID!, $title: String!, $description: String, $priority: TodoPriorityEnum!, $status: TodoStatusEnum!) {
            createTodo(categoryId: $categoryId, title: $title, description: $description, priority: $priority, status: $status) {
              id
              categoryId
              title
              description
              priority
              status
            }
          }
        `,
        variables: {
          categoryId: categoryId,
          title: todo.title,
          description: todo.description,
          priority: todo.priority,
          status: todo.status
        }
      }
    );
  }

  updateTodo(todo: TodoModel): Observable<MutationResult<{updateTodo: TodoModel}>> {
    return this.apollo.mutate<{updateTodo: TodoModel}>({
        mutation: gql`
          mutation updateTodo($id: ID!, $title: String, $description: String, $priority: TodoPriorityEnum, $status: TodoStatusEnum) {
            updateTodo(id: $id, title: $title, description: $description, priority: $priority, status: $status) {
              id
              title
              description
              priority
              status
              categoryId
            }
          }
        `,
        variables: {
          id: todo.id,
          title: todo.title,
          description: todo.description,
          priority: todo.priority,
          status: todo.status
        }
      }
    );
  }

  updateTodoStatus(todo: {
    dueDate: Date;
    description: string;
    id: number;
    title: string;
    priority: "LOW" | "MEDIUM" | "HIGH";
    categoryId: number;
    status: "TO_START" | "IN_PROGRESS" | "COMPLETED"
  }): Observable<MutationResult<{ updateTodo: TodoModel }>> {
    const isoDate = todo.dueDate ? new Date(todo.dueDate).toISOString() : null;
    return this.apollo.mutate<{updateTodo: TodoModel}>({
        mutation: gql`
          mutation updateTodo($id: ID!, $title: String, $description: String, $dueDate: String, $priority: TodoPriorityEnum, $status: TodoStatusEnum) {
            updateTodo(id: $id, title: $title, description: $description, dueDate: $dueDate, priority: $priority, status: $status) {
              id
              title
              description
              dueDate
              priority
              status
              categoryId
            }
          }
        `,
        variables: {
          id: todo.id,
          title: todo.title,
          description: todo.description,
          dueDate: isoDate,
          priority: todo.priority,
          status: todo.status
        }
      }
    );
  }

  updateTodoPriority(todo: {
    dueDate: Date;
    description: string;
    id: number;
    title: string;
    priority: "LOW" | "MEDIUM" | "HIGH";
    categoryId: number;
    status: "TO_START" | "IN_PROGRESS" | "COMPLETED"
  }): Observable<MutationResult<{ updateTodo: TodoModel }>> {
    const isoDate = todo.dueDate ? new Date(todo.dueDate).toISOString() : null;
    return this.apollo.mutate<{updateTodo: TodoModel}>({
        mutation: gql`
          mutation updateTodo($id: ID!, $title: String, $description: String, $dueDate: String, $priority: TodoPriorityEnum, $status: TodoStatusEnum) {
            updateTodo(id: $id, title: $title, description: $description, dueDate: $dueDate, priority: $priority, status: $status) {
              id
              title
              description
              dueDate
              priority
              status
              categoryId
            }
          }
        `,
        variables: {
          id: todo.id,
          title: todo.title,
          description: todo.description,
          dueDate: isoDate,
          priority: todo.priority,
          status: todo.status
        }
      }
    );
  }

  deleteTodo(todoId: number, todo: TodoModel): Observable<MutationResult<{deleteTodo: TodoModel}>> {
    return this.apollo.mutate<{deleteTodo: TodoModel}>({
        mutation: gql`
          mutation deleteTodo($id: ID!) { deleteTodo(id: $id) }
        `,
        variables: {
          id: todoId,
        }
      }
    );
  }
}
